﻿using HarryPotterKata.Discount;
using HarryPotterKata.Discount.Definitions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HarryPotterKata
{
    public class CartPricer
    {
        private List<DiscountDefinition> availableDiscountsDefinitions = new List<DiscountDefinition>
            {
                new NoDiscount(),
                new TwoBooksDiscount(),
                new ThreeBooksDiscount(),
                new FourBooksDiscount(),
                new FiveBooksDiscount(),
            };

        public Price ComputePrice(Cart cart)
        {
            var bestDiscountAmount = FindBestDiscounts(cart);

            var pricePerBook = Price.PerBook();
            return pricePerBook.ApplyDiscounts(bestDiscountAmount);
        }

        private Discounts FindBestDiscounts(Cart cart)
        {
            if (cart.IsEmpty())
            {
                return Discounts.Empty();
            }

            var applicableDiscountsDefinition = GetApplicableDiscountsDefinition(cart);

            var bestDiscounts = Discounts.Max();

            foreach (var discountDefinition in applicableDiscountsDefinition)
            {
                var possibleSetsOfBooksWithADiscount = discountDefinition.DiscountedSetsOfBooks(cart);

                var discounts = possibleSetsOfBooksWithADiscount.Discounts();

                var remainingCart = possibleSetsOfBooksWithADiscount.NewCartWithNotDiscountedBooks();

                discounts.AddAll(FindBestDiscounts(remainingCart));

                bestDiscounts = new[] { discounts, bestDiscounts }.Min();
            }

            return bestDiscounts;
        }

        private IEnumerable<DiscountDefinition> GetApplicableDiscountsDefinition(Cart cart)
        {
            Func<DiscountDefinition, bool> isDiscountApplicable = discount => discount.IsApplicable(cart);
            return availableDiscountsDefinitions.Where(isDiscountApplicable);
        }
    }
}
