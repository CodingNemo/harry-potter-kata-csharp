﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace HarryPotterKata.Decomposition
{
    public class Decomposition<TElement> : IEnumerable<IEnumerable<TElement>>
    {
        private DecomposedSets sets = new DecomposedSets();

        private DecompositionStrategy strategy;

        public Decomposition(DecompositionStrategy strategy)
        {
            this.strategy = strategy;
        }

        public void Add(TElement element)
        {
            strategy.Decompose(element, sets);
        }

        public IEnumerator<IEnumerable<TElement>> GetEnumerator()
        {
            return sets.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public abstract class DecompositionStrategy
        {
            public abstract void Decompose(TElement element, DecomposedSets sets);
        }

        public class DecomposedSets : IEnumerable<IEnumerable<TElement>>
        {
            private List<DecomposedSet> sets = new List<DecomposedSet>();

            public IEnumerator<IEnumerable<TElement>> GetEnumerator()
            {
                return sets.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

            private void Add(DecomposedSet decomposed)
            {
                sets.Add(decomposed);
            }

            public DecomposedSet FindOrCreate(Predicate<DecomposedSet> predicate)
            {
                var existingSet = sets.FirstOrDefault(s => predicate(s));

                if (existingSet != null)
                {
                    return existingSet;
                }

                var newSet = new DecomposedSet();
                sets.Add(newSet);
                return newSet;
            }

            public class DecomposedSet : IEnumerable<TElement>
            {
                private List<TElement> elements = new List<TElement>();

                public void Add(TElement element)
                {
                    elements.Add(element);
                }

                public bool HasSize(int size)
                {
                    return elements.Count == size;
                }

                public IEnumerator<TElement> GetEnumerator()
                {
                    return elements.GetEnumerator();
                }

                IEnumerator IEnumerable.GetEnumerator()
                {
                    return GetEnumerator();
                }
            }
        }
    }
}
