﻿using System;
using System.Linq;

namespace HarryPotterKata.Decomposition
{
    public class DecomposeBySizeAndUnicityStrategy<TElement> : Decomposition<TElement>.DecompositionStrategy
    {
        private int size;

        public DecomposeBySizeAndUnicityStrategy(int size)
        {
            this.size = size;
        }

        public override void Decompose(TElement element, Decomposition<TElement>.DecomposedSets sets)
        {
            Predicate<Decomposition<TElement>.DecomposedSets.DecomposedSet> doesNotContainsElement =
                s => !s.Contains(element);

            Predicate<Decomposition<TElement>.DecomposedSets.DecomposedSet> hasNotReachedMaximumSize =
                s => !s.HasSize(size);

            var set = sets.FindOrCreate(s => doesNotContainsElement(s) && hasNotReachedMaximumSize(s));
            set.Add(element);
        }
    }
}
