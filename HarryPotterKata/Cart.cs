﻿using HarryPotterKata.Decomposition;
using System.Collections.Generic;
using System.Linq;

namespace HarryPotterKata
{
    public class Cart
    {
        private List<BookTitle> books = new List<BookTitle>();

        public bool IsEmpty()
        {
            return !books.Any();
        }

        public Cart Add(params BookTitle[] titles)
        {
            books.AddRange(titles);
            return this;
        }

        public Cart Add(IEnumerable<BookTitle> titles)
        {
            return Add(titles.ToArray());
        }

        public bool HasAtLeastDifferentBookTitles(int numberOfBookTitles)
        {
            var distinctBookTitles = books.Distinct();
            return distinctBookTitles.Count() >= numberOfBookTitles;
        }

        public IEnumerable<SetOfBooks> DecomposeIntoSetsOfDistinctBookTitles(int setSize)
        {
            var decomposition = new Decomposition<BookTitle>(
                new DecomposeBySizeAndUnicityStrategy<BookTitle>(setSize));

            foreach (var book in books)
            {
                decomposition.Add(book);
            }

            return decomposition.Select(d => new SetOfBooks(d));
        }        
    }
}
