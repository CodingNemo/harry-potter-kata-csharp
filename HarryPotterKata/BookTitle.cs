﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HarryPotterKata
{
    public enum BookTitle
    {
        PhilosopherStone,
        ChamberOfSecret,
        PrisonerOfAskaban,
        GobletOfFire,
        OrderOfPhoenix,
        HalfBloodPrince,
        DeathlyHallows
    }
}
