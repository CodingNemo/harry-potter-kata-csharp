﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace HarryPotterKata
{
    public class SetOfBooks : IEnumerable<BookTitle>
    {
        private List<BookTitle> books;

        public SetOfBooks(IEnumerable<BookTitle> books)
        {
            this.books = books.ToList();
        }

        public bool HasSize(int expectedSize)
        {
            return books.Count == expectedSize;
        }

        public IEnumerator<BookTitle> GetEnumerator()
        {
            return books.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public override string ToString()
        {
            return $"Set of {books.Count} Books";
        }
    }
}