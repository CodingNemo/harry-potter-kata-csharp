﻿namespace HarryPotterKata.Discount.Definitions
{
    public class FourBooksDiscount : DiscountDefinition
    {
        public FourBooksDiscount()
            : base(4, 20) { }
    }
}
