﻿using System.Linq;

namespace HarryPotterKata.Discount.Definitions
{
    public class DiscountDefinition
    {
        private Percent discountAmount;
        private int numberOfBookTitleRequiredToApplyDiscount;

        protected DiscountDefinition(int numberOfDifferentBookTitles, Percent discountAmount)
        {
            numberOfBookTitleRequiredToApplyDiscount = numberOfDifferentBookTitles;
            this.discountAmount = discountAmount;
        }

        public bool IsApplicable(Cart cart)
        {
            return cart.HasAtLeastDifferentBookTitles(numberOfBookTitleRequiredToApplyDiscount);
        }

        public DiscountedSetsOfBooks DiscountedSetsOfBooks(Cart cart)
        {
            var decomposition = cart.DecomposeIntoSetsOfDistinctBookTitles(numberOfBookTitleRequiredToApplyDiscount);
            return new DiscountedSetsOfBooks(decomposition.Select(BuildDiscountedSetOfBooks));
        }

        private DiscountedSetOfBooks BuildDiscountedSetOfBooks(SetOfBooks set)
        {
            if (set.HasSize(numberOfBookTitleRequiredToApplyDiscount))
            {
                var discountValue = numberOfBookTitleRequiredToApplyDiscount * discountAmount.ComplementaryRatioValue();
                return new DiscountedSetOfBooks(discountValue, set);
            }

            return new NotDiscountedSetOfBooks(set);
        }

        public override string ToString()
        {
            return $"Discount of {discountAmount} when {numberOfBookTitleRequiredToApplyDiscount} different titles.";
        }
    }
}
