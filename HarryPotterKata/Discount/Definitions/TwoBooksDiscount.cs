﻿namespace HarryPotterKata.Discount.Definitions
{
    public class TwoBooksDiscount : DiscountDefinition
    {
        public TwoBooksDiscount()
            : base(2, 5) { }
    }
}
