﻿namespace HarryPotterKata.Discount.Definitions
{
    public class FiveBooksDiscount : DiscountDefinition
    {
        public FiveBooksDiscount()
            : base(5, 25) { }
    }
}
