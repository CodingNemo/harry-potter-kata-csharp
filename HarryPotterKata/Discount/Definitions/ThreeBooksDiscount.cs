﻿namespace HarryPotterKata.Discount.Definitions
{
    public class ThreeBooksDiscount : DiscountDefinition
    {
        public ThreeBooksDiscount()
            : base(3, 10) { }
    }
}
