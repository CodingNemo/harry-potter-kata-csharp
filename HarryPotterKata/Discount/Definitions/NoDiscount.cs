﻿namespace HarryPotterKata.Discount.Definitions
{
    public class NoDiscount : DiscountDefinition
    {
        public NoDiscount()
            : base(1, 0) { }

        public override string ToString()
        {
            return "No Discount";
        }
    }
}
