﻿using System.Collections;
using System.Collections.Generic;

namespace HarryPotterKata.Discount
{
    public class DiscountedSetOfBooks : IEnumerable<BookTitle>
    {
        private decimal discountAmount;

        protected SetOfBooks books;

        public DiscountedSetOfBooks(decimal discountAmount, SetOfBooks setOfBooks)
        {
            this.discountAmount = discountAmount;
            books = setOfBooks;
        }
        
        internal void Aggregate(DiscountTotal total)
        {
            total.Add(discountAmount);
        }

        public override string ToString()
        {
            return $"{books} with a Discount of {discountAmount}";
        }

        public virtual void Fill(Discounts discounts)
        {
            discounts.Add(this);
        }

        public virtual void Fill(Cart cart) { }

        public IEnumerator<BookTitle> GetEnumerator()
        {
            return books.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}