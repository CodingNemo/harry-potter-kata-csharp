﻿using System;

namespace HarryPotterKata.Discount
{
    public class DiscountTotal : IEquatable<DiscountTotal>, IComparable<DiscountTotal>
    {
        private decimal total;

        public void Add(decimal value)
        {
            total += value;
        }

        public int CompareTo(DiscountTotal other)
        {
            var otherTotal = other.total;
            return total.CompareTo(otherTotal);
        }

        public override string ToString()
        {
            return $"Discount total : {total}";
        }

        public override int GetHashCode()
        {
            return total.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return Equals((DiscountTotal)obj);
        }

        public bool Equals(DiscountTotal other)
        {
            return total == other.total;
        }

        public decimal Times(decimal amount)
        {
            return amount * total;
        }
    }
}
