﻿using HarryPotterKata.Discount;
using System;

namespace HarryPotterKata
{
    public class Price : IEquatable<Price>
    {
        private decimal amount;

        protected Price(decimal amount)
        {
            this.amount = amount;
        }

        public static Price PerBook()
        {
            return new BookPrice();
        }

        public static implicit operator Price(decimal price)
        {
            return new Price(price);
        }

        internal Price ApplyDiscounts(Discounts discounts)
        {
            return new Price(discounts.ComputeFullDiscount(amount));
        }

        public override bool Equals(object other)
        {
            var otherPrice = other as Price;
            return Equals(otherPrice);
        }

        public override int GetHashCode()
        {
            return amount.GetHashCode();
        }

        public bool Equals(Price other)
        {
            return other != null && other.amount == amount;
        }

        public override string ToString()
        {
            return amount.ToString();
        }

        private class BookPrice : Price
        {
            public BookPrice()
                : base(8)
            {
            }
        }
    }
}