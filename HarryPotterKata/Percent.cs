﻿namespace HarryPotterKata
{
    public class Percent
    {
        private decimal amount;

        private Percent(decimal amount)
        {
            this.amount = amount;
        }
        
        public static implicit operator Percent(decimal amount)
        {
            return new Percent(amount);
        }

        private decimal RatioValue()
        {
            return amount / 100;
        }

        public decimal ComplementaryRatioValue()
        {
            return 1 - RatioValue();
        }

        public override string ToString()
        {
            return $"{amount}%";
        }
    }
}