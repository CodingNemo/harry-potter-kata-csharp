﻿using NFluent;
using NUnit.Framework;

namespace HarryPotterKata
{
    using static BookTitle;

    public class CartShould
    {
        private const decimal BookPrice = 8m;

        private const decimal NoDiscountValue = 1;
        private const decimal TwoBooksDiscountValue = 2 * (1 - 0.05m);
        private const decimal ThreeBooksDiscountValue = 3 * (1 - 0.10m);
        private const decimal FourBooksDiscountValue = 4 * (1 - 0.20m);
        private const decimal FiveBooksDiscountValue = 5 * (1 - 0.25m);

        private CartPricer pricer;
        private Cart cart;

        [SetUp]
        public void SetUp()
        {
            pricer = new CartPricer();
            cart = new Cart();
        }

        [Test]
        public void Apply_the_same_price_when_containing_an_order_six_books_with_the_same_title()
        {
            cart.Add(
                PhilosopherStone, PhilosopherStone, PhilosopherStone, 
                PhilosopherStone, PhilosopherStone, PhilosopherStone);

            var price = pricer.ComputePrice(cart);

            Check.That(price).IsEqualTo(BookPrice * 6 * NoDiscountValue);
        }

        [Test]
        public void Apply_a_five_percent_discount_when_containing_an_order_with_two_books_with_two_different_titles()
        {
            cart.Add(PhilosopherStone, ChamberOfSecret);

            var price = pricer.ComputePrice(cart);

            Check.That(price).IsEqualTo(BookPrice * TwoBooksDiscountValue);
        }

        [Test]
        public void Apply_the_five_percent_discount_only_on_the_two_books_with_different_titles()
        {
            cart.Add(PhilosopherStone, ChamberOfSecret)
                .Add(PhilosopherStone, ChamberOfSecret)
                .Add(ChamberOfSecret);

            var price = pricer.ComputePrice(cart);
            Check.That(price).IsEqualTo(BookPrice * (TwoBooksDiscountValue + TwoBooksDiscountValue + NoDiscountValue));
        }

        [Test]
        public void Apply_a_ten_percent_discount_when_containing_an_order_with_three_books_with_three_different_titles()
        {
            cart.Add(PhilosopherStone, ChamberOfSecret, PrisonerOfAskaban);

            var price = pricer.ComputePrice(cart);

            Check.That(price).IsEqualTo(BookPrice * ThreeBooksDiscountValue);
        }

        [Test]
        public void Apply_the_ten_percent_discount_only_on_the_three_books_with_different_titles()
        {
            cart.Add(PhilosopherStone, ChamberOfSecret, PrisonerOfAskaban)
                .Add(PhilosopherStone);

            var price = pricer.ComputePrice(cart);

            Check.That(price).IsEqualTo(BookPrice * (ThreeBooksDiscountValue + NoDiscountValue));
        }

        [Test]
        public void Apply_a_twenty_percent_discount_when_containing_an_order_with_four_books_with_four_different_titles()
        {
            cart.Add(
                PhilosopherStone, ChamberOfSecret, 
                PrisonerOfAskaban, GobletOfFire);

            var price = pricer.ComputePrice(cart);
            
            Check.That(price).IsEqualTo(BookPrice * FourBooksDiscountValue);
        }

        [Test]
        public void Apply_the_twenty_percent_discount_only_on_the_four_books_with_different_titles()
        {
            cart.Add(
                    PhilosopherStone, ChamberOfSecret,
                    PrisonerOfAskaban, GobletOfFire)
                .Add(GobletOfFire);

            var price = pricer.ComputePrice(cart);

            Check.That(price).IsEqualTo(BookPrice * (FourBooksDiscountValue + NoDiscountValue));
        }

        [Test]
        public void Apply_a_twenty_five_percent_discount_when_containing_an_order_with_five_books_with_five_different_titles()
        {
            cart.Add(
                PhilosopherStone, ChamberOfSecret, PrisonerOfAskaban, 
                GobletOfFire, OrderOfPhoenix);

            var price = pricer.ComputePrice(cart);

            Check.That(price).IsEqualTo(BookPrice * FiveBooksDiscountValue);
        }

        [Test]
        public void Apply_a_twenty_five_percent_discount_only_to_the_five_books_with_different_titles()
        {
            cart.Add(
                PhilosopherStone, ChamberOfSecret, PrisonerOfAskaban,
                GobletOfFire, OrderOfPhoenix)
                .Add(OrderOfPhoenix)
                .Add(OrderOfPhoenix);

            var price = pricer.ComputePrice(cart);

            var expectedDiscountValue = (FiveBooksDiscountValue + NoDiscountValue + NoDiscountValue);

            Check.That(price).IsEqualTo(BookPrice * expectedDiscountValue);
        }

        [Test]
        public void Apply_the_best_discount_in_favor_to_the_customer()
        {
            cart.Add(PhilosopherStone, PhilosopherStone)
                .Add(ChamberOfSecret, ChamberOfSecret)
                .Add(PrisonerOfAskaban, PrisonerOfAskaban)
                .Add(GobletOfFire)
                .Add(OrderOfPhoenix);

            var price = pricer.ComputePrice(cart);

            Check.That(price).IsEqualTo(BookPrice * (FourBooksDiscountValue + FourBooksDiscountValue));
        }
    }
}
